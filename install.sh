#!/bin/bash
####################################################################
# Monit installer
# This script help you to install and configure Monit on a Ubuntu server
# Author: Nick Hartjes <nickhartjes@gmail.com>
# Version: 0.1
# Created: Jan 8, 2014 
####################################################################

readonly base_file=`readlink -f "$0"`
readonly base_path=`dirname $base_file`
if [ ! -f $base_path/config ]; then
	echo "Error: You must to create a '$base_path/config' file before, you can create it from '$base_path/config.sample' "
	exit
fi

. "$base_path/config"
. "$base_path/lib/core.sh"
. "$base_path/lib/monit.sh"

printMenu

