#!/bin/bash
####################################################################
# Configuration File
# Author: Nick Hartjes <nickhartjes@gmail.com>
# Version: 0.1
# Created: Jan 8, 2014 
####################################################################


cyan='\e[36m'
red='\e[31m'
endColor='\e[39m'
datetime=$(date +%Y%m%d%H%M%S)


lowercase(){
	echo "$1" | sed "y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/"
}

####################################################################
# Get information about system
####################################################################
shootProfile(){
	OS=`lowercase \`uname\``
	KERNEL=`uname -r`
	MACH=`uname -m`

	if [ "${OS}" == "windowsnt" ]; then
		OS=windows
	elif [ "${OS}" == "darwin" ]; then
		OS=mac
	else
		OS=`uname`
		if [ "${OS}" = "SunOS" ] ; then
			OS=Solaris
			ARCH=`uname -p`
			OSSTR="${OS} ${REV}(${ARCH} `uname -v`)"
		elif [ "${OS}" = "AIX" ] ; then
			OSSTR="${OS} `oslevel` (`oslevel -r`)"
		elif [ "${OS}" = "Linux" ] ; then
			if [ -f /etc/redhat-release ] ; then
				DistroBasedOn='RedHat'
				DIST=`cat /etc/redhat-release |sed s/\ release.*//`
				PSUEDONAME=`cat /etc/redhat-release | sed s/.*\(// | sed s/\)//`
				REV=`cat /etc/redhat-release | sed s/.*release\ // | sed s/\ .*//`
			elif [ -f /etc/SuSE-release ] ; then
				DistroBasedOn='SuSe'
				PSUEDONAME=`cat /etc/SuSE-release | tr "\n" ' '| sed s/VERSION.*//`
				REV=`cat /etc/SuSE-release | tr "\n" ' ' | sed s/.*=\ //`
			elif [ -f /etc/mandrake-release ] ; then
				DistroBasedOn='Mandrake'
				PSUEDONAME=`cat /etc/mandrake-release | sed s/.*\(// | sed s/\)//`
				REV=`cat /etc/mandrake-release | sed s/.*release\ // | sed s/\ .*//`
			elif [ -f /etc/debian_version ] ; then
				DistroBasedOn='Debian'
				if [ -f /etc/lsb-release ] ; then
			        	DIST=`cat /etc/lsb-release | grep '^DISTRIB_ID' | awk -F=  '{ print $2 }'`
			                PSUEDONAME=`cat /etc/lsb-release | grep '^DISTRIB_CODENAME' | awk -F=  '{ print $2 }'`
			                REV=`cat /etc/lsb-release | grep '^DISTRIB_RELEASE' | awk -F=  '{ print $2 }'`
            			fi
			fi
			if [ -f /etc/UnitedLinux-release ] ; then
				DIST="${DIST}[`cat /etc/UnitedLinux-release | tr "\n" ' ' | sed s/VERSION.*//`]"
			fi
			OS=`lowercase $OS`
			DistroBasedOn=`lowercase $DistroBasedOn`
		 	readonly OS
		 	readonly DIST
			readonly DistroBasedOn
		 	readonly PSUEDONAME
		 	readonly REV
		 	readonly KERNEL
		 	readonly MACH
		fi

	fi
}
shootProfile
echo "OS: $OS"
echo "DIST: $DIST"
echo "PSUEDONAME: $PSUEDONAME"
echo "REV: $REV"
echo "DistroBasedOn: $DistroBasedOn"
echo "KERNEL: $KERNEL"
echo "MACH: $MACH"
echo "========"



####################################################################
# Print Menu
####################################################################
printMenu(){
	if [ "$deamonInterval" = "" || "$mailAdmin" = "" || "$port" = "" ]; then
		echo -e "$red Error: Settings in the config file can't be empty $endColor"
		echo ""
		echo ""
		exit 1 
	fi
	#clear
	echo -e "$cyan Fast and Easy Munin Installation $endColor"
	echo " What do you want to do?"
	echo -e "\t1) Install munin"
	echo -e "\t2) Install munin alert for SSH"
	echo -e "\t3) Install contact"
	echo -e "\t4) Install diskspace monitor"
	echo -e "\t5) Install MongD monitor"
	echo -e "\t6) Install MongoC monitor"
	echo -e "\t7) Install cpu monitor"
	echo -e "\t99) I do not know, exit!"
	read option;
	runOption
}
####################################################################
# Run an Option
####################################################################
runOption(){
	case $option in
		1) installMonit;;
		2) monitSsh;;
		3) monitContact;;
		4) monitDiskspace;;
		5) monitMongoD;;
		6) monitMongoC;;
		7) monitDefault;;
		99) exit
	esac 
	echo "Press any Key to continue"
	read x
	printMenu
}


