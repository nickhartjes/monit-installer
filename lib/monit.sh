#!/bin/bash
####################################################################
# Configuration File
# Author: Nick Hartjes <nickhartjes@gmail.com>
# Version: 0.1
# Created: Jan 8, 2014 
####################################################################

####################################################################
# Port to conect by SSH
####################################################################
portSsh='22'

####################################################################
# Admin's email
####################################################################
mailAdmin='n.hartjes@fuuzle.com'

####################################################################
# Monit settings
####################################################################
deamonInterval='60'

####################################################################
# Mongo settings
####################################################################
mongoDPort='27018'
mongoCPort='27018'

####################################################################
# Vars
####################################################################
echo "monit functions were loaded"
confBasePath='/etc/monit/conf.d'
monitconf='/etc/monit/monitrc'
hostname=$(hostname)
hostname="$hostname.fuuzle.com"

echo $hostname

####################################################################
# Install Monit
####################################################################
installMonit(){
	if [ ! `whoami` = "root" ]; then 
		echo -e "$red Error: You must to be ROOT user to run this function $endColor"
		return
	fi
	echo -e "$cyan============================ Install Monit =============================$endColor"

	#apt-get -y update
	apt-get -y install monit

	if [ "$deamonInterval" != "120" ]; then
		sed '/^set daemon 120.*/ s/^set daemon 120/set daemon $deamonInterval /' $monitconf > tmp
		echo $tmp
		sudo cat tmp > $monitconf
	fi

	sudo chmod 700 $monitconf

	monitDefault

	echo -e "$cyan==================== Monit installed ====================$endColor"
}
####################################################################
# Monit Default settings
####################################################################
monitDefault(){
	file=$confBasePath/default.conf

	##Insert code into the file
echo '
check system $hostname
	if loadavg (1min) > 4 then alert
	if loadavg (5min) > 2 then alert
	if memory usage > 75% then alert
	if swap usage > 25% then alert
	if cpu usage (user) > 70% then alert
	if cpu usage (system) > 30% then alert
	if cpu usage (wait) > 20% then alert	

check device var with path /var
	if SPACE usage > 80% then alert

# Testmail
check file alerttest with path /.nonexistent
	alert $mailAdmin with reminder on 500 cycles    	

    	' >> $file

	##when done, set permissions
	sudo chmod 775 $file

	sudo service monit restart
}
####################################################################
# Monit SSH
####################################################################
monitSsh(){
	file=$confBasePath/ssh.conf

	##Insert code into the file
	echo 'check process sshd with pidfile /var/run/sshd.pid
	start program "/etc/init.d/ssh start"
	stop program "/etc/init.d/ssh stop"
	if failed host 127.0.0.1 port $portSsh protocol ssh then restart
	if 5 restarts within 5 cycles then timeout' > $file

	##when done, set permissions
	sudo chmod 775 $file

	sudo service monit restart
}

####################################################################
# Contact
####################################################################
monitContact(){
	file=$confBasePath/contact.conf

	##Insert code into the file
	echo 'set mailserver smtp.gmail.com port 587
    username "nickhartjes@gmail.com" password "iqrlfuozkoiprsei"
    using tlsv1
    with timeout 30 seconds

	set alert $mailAdmin with reminder on 360 cycles

	set mail-format {
	from: monit@$HOST
	subject: monit alert -- @HOST $EVENT $SERVICE
	message: 
		$EVENT Service $SERVICE
		Date:        $DATE
		Action:      $ACTION
		Host:        $HOST
		Description: $DESCRIPTION

	}' > $file

	##when done, set permissions
	sudo chmod 775 $file

	sudo service monit restart
}

####################################################################
# Diskspace
####################################################################
monitDiskspace(){
	file=$confBasePath/diskspace.conf

	##Insert code into the file
	echo 'check filesystem rootfs with path /
	if space usage > 80% then alert' > $file

	##when done, set permissions
	sudo chmod 775 $file

	sudo service monit restart
}

####################################################################
# MongoD
####################################################################
monitMongoD(){
	file=$confBasePath/mongoD.conf

	##Insert code into the file
	echo 'check process mongodb with pidfile /var/lib/mongodb/mongod.lock 
	start program = "/sbin/start mongodb" 
	stop program = "/sbin/stop mongodb" 
	if failed host 127.0.0.1 port $mongoDPort then restart if 5 restart 
	within 5 cycles then timeout' > $file

	##when done, set permissions
	sudo chmod 775 $file

	sudo service monit restart
}

####################################################################
# MongoC
####################################################################
monitMongoC(){
	file=$confBasePath/mongoC.conf

	##Insert code into the file
	echo 'check process mongodb with pidfile /var/lib//mongodbconfigsrv/mongod.lock 
	start program = "/sbin/start mongodb" 
	stop program = "/sbin/stop mongodb" 
	if failed host 127.0.0.1 port $mongoCPort then restart if 5 restart 
	within 5 cycles then timeout' > $file

	##when done, set permissions
	sudo chmod 775 $file

	sudo service monit restart
}

